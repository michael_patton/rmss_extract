create or replace PACKAGE BODY RMSS_EXTRACT AS

  -- ***************************************************************************************

  function get_rmss_xref(p_kind     in rmss_riskware_xref.kind%type,
                         p_old_code in rmss_riskware_xref.old_code%type,
                         p_old_desc in rmss_riskware_xref.old_description%type) return rmss_riskware_xref.new_code%type AS
    l_return varchar2(200);
  BEGIN

    BEGIN
      -- look for value based on code
      SELECT x.new_code
        INTO l_return
        FROM rmss_riskware_xref x
       WHERE x.kind = p_kind
         AND x.old_code = p_old_code;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN 
         -- look for value based on description only
         l_return := get_rmss_xref_desc(p_kind     => p_kind,
                                        p_old_desc => p_old_desc);
                                        
      WHEN TOO_MANY_ROWS THEN 
        l_return := 'MULT'||':'||p_old_code||':'||p_old_desc;
    END;

    RETURN l_return;
  END get_rmss_xref;

  -- ***************************************************************************************

  function get_rmss_xref_desc(p_kind     in rmss_riskware_xref.kind%type,
                              p_old_desc in rmss_riskware_xref.OLD_DESCRIPTION%type) return rmss_riskware_xref.new_code%type AS
    l_return varchar2(200);
  begin
    BEGIN
      SELECT x.new_code
        INTO l_return
        FROM rmss_riskware_xref x
       WHERE x.kind = p_kind
         AND OLD_DESCRIPTION  = rtrim(p_old_desc, chr(10)||chr(13));

    EXCEPTION
      WHEN TOO_MANY_ROWS THEN 
        l_return := 'MULT'||':'||p_old_desc;
      WHEN NO_DATA_FOUND THEN 
        l_return := NULL;
        --l_return := 'NONE';
    END;

    RETURN l_return;  
  end;
  
  -- ***************************************************************************************
  
  FUNCTION get_location_code(p_parent_loc  IN VARCHAR2,
                             p_loc_details IN VARCHAR2) RETURN rmss_rw_locations.locationcode%TYPE AS

    l_loc_details varchar2(2000);
     
    cursor cur_loc is
    select * 
    from (
       -- match incident location + type + name
       select 100 as priority, rwl.*
            FROM rmss_rw_locations rwl
           WHERE 1=1
             AND rwl.parentlocationcode = p_parent_loc
             AND REGEXP_LIKE(l_loc_details,
                             rwl.locationtypecode||'( |)'||substr(rwl.locationname,1,3) )
        UNION
       -- as above, match incident location + type + name (different naming convention for carparks)        
       select 150 as priority, rwl.*
            FROM rmss_rw_locations rwl
           WHERE 1=1
             AND rwl.parentlocationcode = p_parent_loc
             AND rwl.locationtypecode = 'CARPARK'
             AND REGEXP_LIKE(l_loc_details,
                             UPPER(rwl.locationname) )
        UNION       
        -- type + name
       select 200, rwl.*
            FROM rmss_rw_locations rwl
           WHERE 1=1
             AND REGEXP_LIKE(l_loc_details,
                             rwl.locationtypecode||'( |)'||substr(rwl.locationname,1,3) )
        UNION
        -- variations on type + name
          SELECT 250 as priority, rwl.*
            FROM rmss_rw_locations rwl
           WHERE 1=1
             AND regexp_like(l_loc_details,
                             '^'||substr(rwl.locationname,1,3) )
        UNION
        -- variations on type + name
          SELECT 300 as priority, rwl.*
            FROM rmss_rw_locations rwl
           WHERE 1=1
             AND regexp_like(l_loc_details,
                             '(B|Bld|BLD|)( |)'||substr(rwl.locationname,1,3) )
        )                     
        order by priority, 
                 case parentlocationcode
                      WHEN n'BENTL' THEN 100
                      WHEN n'TCHPK' THEN 200
                      WHEN n'KALGO' THEN 300
                      WHEN n'SHNPK' THEN 400--'
                      ELSE 1000
                    END,                 
                 case locationtypecode
                      when n'BUILDING' THEN 100
                      WHEN n'CARPARK'  THEN 200--'
                      ELSE 1000
                    END
                    ;
    l_return cur_loc%rowtype;
        
  BEGIN
      l_loc_details := UPPER(p_loc_details);
      
      open cur_loc;
      fetch cur_loc into l_return;
      close cur_loc;
    
    return(l_return.locationcode||'/'||l_return.priority);
    
  END;
  
  
                                          
  -- ***************************************************************************************
                                          
  function get_ID_from_name(p_per_type    in varchar2,
                            p_firstname   in person.firstname%type,
                            p_lastname    in person.lastname%type) return person.internalstaffid%type AS
    l_return person.internalstaffid%type;
    x_contractor exception;
    x_visitor exception;
  begin
  
    BEGIN     
    
      -- check for the exact name and person type
      SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE TRIM(LOWER(ud.given_name)) = TRIM(LOWER(p_firstname))
         AND TRIM(LOWER(ud.family_name)) = TRIM(LOWER(p_lastname))
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;

    EXCEPTION
      WHEN TOO_MANY_ROWS THEN 
        l_return := 'MULT_Exact';
      WHEN NO_DATA_FOUND THEN 
        l_return := NULL;
        --l_return := 'NONE';       
    END;


    -- exact match using preferred first name
    if l_return is null then 
      begin
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE TRIM(LOWER(ud.preferred_name)) = TRIM(LOWER(p_firstname))
         AND TRIM(LOWER(ud.family_name)) = TRIM(LOWER(p_lastname))
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Pref';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    
    -- do a high similarity check (correct for typos)
    if l_return is null then 
      begin
        
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE (utl_match.jaro_winkler(trim(LOWER(ud.given_name)), trim(LOWER(p_firstname)) ) + utl_match.jaro_winkler(trim(LOWER(ud.family_name)), trim(LOWER(p_lastname)) ) > 1.9 )
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Simil';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    

      
    -- look for match on name only regardless of person type
    if l_return is null then 
      begin
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE TRIM(LOWER(ud.given_name)) = TRIM(LOWER(p_firstname))
         AND TRIM(LOWER(ud.family_name)) = TRIM(LOWER(p_lastname));
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Name';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    
    end if;

    -- do a soundex check with person type
    if l_return is null then 
      begin       
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE SOUNDEX(ud.given_name) = SOUNDEX(p_firstname)
         AND SOUNDEX(ud.family_name) = SOUNDEX(p_lastname)
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Sound';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    
    RETURN l_return;  
  end get_ID_from_name;

  -- ***************************************************************************************
  function get_ID_from_name2(p_per_type   in varchar2,
                            p_firstname   in person.firstname%type,
                            p_lastname    in person.lastname%type) return person.internalstaffid%type AS
    l_return person.internalstaffid%type;
    x_contractor exception;
    x_visitor exception;
  begin
  
    BEGIN     
    
      -- check for the exact name and person type
      SELECT ud.source_user_id
        INTO l_return
        FROM rmss_user_detail ud --user_detail@dogtag ud
       WHERE ud.given_name = TRIM(LOWER(p_firstname))
         AND ud.family_name = TRIM(LOWER(p_lastname))
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;

    EXCEPTION
      WHEN TOO_MANY_ROWS THEN 
        l_return := 'MULT_Exact';
      WHEN NO_DATA_FOUND THEN 
        l_return := NULL;
        --l_return := 'NONE';       
    END;


    -- exact match using preferred first name
    if l_return is null then 
      begin
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE ud.preferred_name = TRIM(LOWER(p_firstname))
         AND ud.family_name = TRIM(LOWER(p_lastname))
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Pref';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    
    -- do a high similarity check (correct for typos)
    if l_return is null then 
      begin
        
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE (utl_match.jaro_winkler(ud.given_name, trim(LOWER(p_firstname)) ) + utl_match.jaro_winkler(ud.family_name, trim(LOWER(p_lastname)) ) > 1.9 )
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Simil';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    

      
    -- look for match on name only regardless of person type
    if l_return is null then 
      begin
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE ud.given_name = TRIM(LOWER(p_firstname))
         AND ud.family_name = TRIM(LOWER(p_lastname));
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Name';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    
    end if;

    -- do a soundex check with person type
    if l_return is null then 
      begin       
        SELECT ud.source_user_id
        INTO l_return
        FROM user_detail@dogtag ud
       WHERE SOUNDEX(ud.given_name) = SOUNDEX(p_firstname)
         AND SOUNDEX(ud.family_name) = SOUNDEX(p_lastname)
         AND ud.user_type_id = CASE p_per_type 
                                 WHEN 'STUDENT' THEN 1
                                 WHEN 'STAFF' THEN 2
                                 ELSE 0
                               END;
         
        EXCEPTION
          WHEN TOO_MANY_ROWS THEN 
            l_return := 'MULT_Sound';
          WHEN NO_DATA_FOUND THEN 
            l_return := NULL; 
      end;
    END IF;
    
    RETURN l_return;  
  end get_ID_from_name2;
  
END RMSS_EXTRACT;