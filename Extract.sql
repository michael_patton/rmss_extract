-- select aoicode,count(9) from (
--select * from (
select 
-- Level_2.incidentid2,  -- for QA only
Level_2.EmployeeID as EmployeeID
--,Level_2.EmployeeID_orig as EmployeeID_orig
,COALESCE(Level_2.EmployeeFirstName, Level_2.EmployeeID) as EmployeeFirstName-- mpatton chloe may want to change default --'
,COALESCE(Level_2.EmployeeSurname, Level_2.EmployeeID) as EmployeeSurname -- mpatton chloe may want to change default --'
,CASE 
  WHEN eoh.location is not null then CAST(eoh.location AS NVARCHAR2(10))
  ELSE n'BENTL'    --  default to bentley '
  END
    as EmployeeLocationCode  
,Level_2.PersonLoggedByID 
--,Level_2.TMP_logged_email_address   -- QA only
,Level_2.PersonAssignedToID 
,Level_2.PersonTypeCode as EmployeePersonTypeCode
,Level_2.WorkActivityCategory as WorkActivityCategory                                             
,Level_2.WorkActivityPerformed
,Level_2.WorkActivitySubCategory
,COALESCE( level_2.LocationCategory, 
           CASE WHEN eoh.location = 'OFFCP' then n'00006' --'Off Campus''
                ELSE NULL 
           END 
         ) AS LocationCategory
,Level_2.LocationDetails
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN NULL  --'
  ELSE Level_2.MajorEvent  
 END AS MajorEvent 
,Level_2.Status as Status
,Level_2.IncidentShortTitle
,to_char(Level_2.DateTimeOfIncident,'DD-MM-YYYY') as DateTimeOfIncident
,to_char(Level_2.DateTimeReported,'DD-MM-YYYY') as DateTimeReported  
,Level_2.MOICode 
,COALESCE(Level_2.AOICode,
null,
          CAST(
          CASE 
            WHEN Level_2.MOICode = '11' THEN '5' 
            WHEN Level_2.MajorEvent = 'Fire/Explosions' THEN '100'
            WHEN Level_2.MajorEvent = 'Vehicle Event' THEN '3'
            WHEN REGEXP_LIKE(Level_2.riskdescription, 'Psychological', 'i') THEN '101'
            WHEN REGEXP_LIKE(Level_2.riskdescription, '(Fainted|Seizure)', 'i') THEN '16'
            WHEN REGEXP_LIKE(Level_2.riskdescription, '(Needle)', 'i') THEN '10'
          END
          AS NVARCHAR2(100) )
          ) AS AOICode
--, Level_2.OLD_NATURE_VALUES 
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN NULL  --'  
  ELSE Level_2.NOICode   
  END AS NOICode 
,CASE Level_2.MajorEvent   
  WHEN n'Hazard' THEN NULL   --'  
  ELSE Level_2.BodilyCode  
  END AS BodilyCode
,COALESCE( level_2.IncidentLocationCode, 
 CAST(
    CASE 
      WHEN level_2.LocationCategory in ( 'Off Campus','00006') THEN  'OFFCP'
      
      WHEN level_2.LocationCategory in ('Grounds','00004') THEN 
         CASE 
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(TECH( |)PARK)' ) THEN 'TCHPK.G'
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(SHENTON( |)PARK)' ) THEN 'SHNPK.G'
           ELSE 'BENTL.G'         
         END
         
      WHEN level_2.LocationCategory in ('Pathway/Walkway','00008') THEN 
         CASE 
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(TECH( |)PARK)' ) THEN 'TCHPK.P'
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(SHENTON( |)PARK)' ) THEN 'SHNPK.P'
           ELSE 'BENTL.P'         
         END
         
      WHEN level_2.LocationCategory in ( 'Roads', '00009') THEN 
         CASE 
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(TECH( |)PARK)' ) THEN 'TCHPK.R'
           WHEN REGEXP_LIKE(UPPER(Level_2.LocationDetails), 
                            '(SHENTON( |)PARK)' ) THEN 'SHNPK.R'
           ELSE 'BENTL.R'         
         END

      WHEN level_2.LocationCategory in ('Carpark', '00002') THEN
         CASE 
           WHEN level_2.carpark_number is null then null
           ELSE  'BENTL.C'||level_2.carpark_number
         END
         
      -- try to process everything else as a building
      ELSE
          CASE 
            WHEN TO_NUMBER(level_2.building_number) between   0 and 599 then 'BENTL.B'||level_2.building_number 
            WHEN TO_NUMBER(level_2.building_number) between 600 and 699 then 'TCHPK.B'||level_2.building_number
            WHEN TO_NUMBER(level_2.building_number) between 700 and 799 then 'KALGO.B'||level_2.building_number
            WHEN TO_NUMBER(level_2.building_number) between 900 and 999 then 'SHNPK.B'||level_2.building_number           
            WHEN REGEXP_LIKE(UPPER(level_2.LocationDetails), '(^CURTIN STADIUM|STADIUM.*INDOOR.*COURT)' ) THEN 'BENTL.B111'
           ELSE NULL
         END
      END 
    AS NVARCHAR2(100) )
 ) 
AS IncidentLocationCode 
,Level_2.RiskDescription 
,Level_2.ProportionOfWork 
,Level_2.TrainingRequired
,Level_2.Witness
,Level_2.ManagerNotified
,Level_2.ManagerNotifiedDateTime
,Level_2.InternalPersonNotified 
,Level_2.InternalPersonNotifiedDateTime
,Level_2.ExternalPersonNotified
,Level_2.ExternalPersonNotifiedDateTime
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.Injury 
  END AS Injury 
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.MedicalTreatment 
  END AS MedicalTreatment 
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.FirstAid 
  END AS FirstAid                                               
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN n'FALSE'  --'  
  ELSE Level_2.GP 
  END AS GP                                   
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN n'FALSE'  --'  
  ELSE Level_2.Hospital 
  END AS Hospital
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN NULL  --'  
  ELSE Level_2.MedicalDetails 
  END AS MedicalDetails
--, Level_2.orig_islti --mpatton
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.NoTimeLost 
  END AS NoTimeLost 
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.LostTimeLost 
  END AS LostTimeLost 
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.RestrictedWorkDuties 
  END AS RestrictedWorkDuties   
,CASE Level_2.MajorEvent  
  WHEN n'Hazard' THEN 'FALSE'  --'  
  ELSE Level_2.Fatality 
  END AS Fatality 
,Level_2.SideOfInjury 
,Level_2.PropertyDamaged
,Level_2.OwnAssetProperty
,Level_2.ThirdPartyAssetProperty
,Level_2.ActionsTaken 
,Level_2.FurtherActions                                                        
,Level_2.PropertyDamageDetail
,Level_2.PropertyDamageCost
,Level_2.EmergencyResponse
,Level_2.WorkSafeNotifiable
,Level_2.EarlyNotification
,COALESCE(eoh.orglevel1code, '5100') as OrgLevel1Code 
,eoh.orglevel2code as OrgLevel2Code
,eoh.orglevel3code as OrgLevel3Code
,eoh.orglevel4code as OrgLevel4Code
,eoh.orglevel5code as OrgLevel5Code
,eoh.orglevel6code as OrgLevel6Code
,null as OrgLevel7Code
,null as OrgLevel8Code
,null as InternalReference
-- new columns as per 12 Sept email
,'N' as EnvironmentalImpactOccurred
,'N' as PrimaryPropertyDamage
,'N' as PrimaryEnvironmentalImpact
,'N' as PrimaryPeopleImpact
,'N' as MultipleTypeAffected
,'N' as SecondaryPeopleImpact
,'N' as SecondaryEnvironmentalImpact
,'N' as SecondaryPropertyDamage
,'N' as EnvironmentalActualImpact
,'N/A' as DamageDescription
FROM (
SELECT DISTINCT
level_1.*
,  COALESCE(level_1.EmployeeID_L1
            ,null
            -- if employeeID is blank then lookup based on name fields
--            ,RMSS_EXTRACT.get_ID_from_name(p_per_type  => level_1.PersonTypeCode,
--                                           p_firstname => level_1.EmployeeFirstName,
--                                           p_lastname  => level_1.EmployeeSurname)
--
            ,CASE level_1.PersonTypeCode  
              WHEN n'VISITOR'    THEN n'VISITOR_ID'  
              WHEN n'CONTRACTOR' THEN n'CONTRACTOR_ID'
              WHEN n'OTHER'      THEN n'OTHER_ID' 
              WHEN n'STAFF'      THEN n'UNKNOWN_STAFF'  
              WHEN n'STUDENT'    THEN n'UNKNOWN_STU'
              WHEN n'* N/A'      THEN n'UNKNOWN_NA' --' --uncomment these for the final extract
              ELSE null
            end
         ) 
 as EmployeeID 
,level_1.EmployeeID_L1 as EmployeeID_orig
,cast(REGEXP_SUBSTR(level_1.LocationDetails,
               '(^|B|BUILDING|BDG|BLD|BLDG)( |)([0-9]{3})',
               1,1,'i',3
              ) as varchar2(100) ) AS building_number
--
,CAST(REGEXP_SUBSTR(level_1.LocationDetails,
                           '(CAR( |)PARK)( |)([a-z]{1}([0-9]{2}|[0-9]{1}))',
                           1,1,'i',4
                          ) AS VARCHAR2(10)) AS carpark_number
FROM (
    SELECT 
    i.incidentid2 as incidentid2,  
    TRIM(SUBSTR(COALESCE(i2a.employeeid,
                         i2d.employeeid,
                         inc_per.internalstaffid, 
                         per.internalstaffid),1,15)) as EmployeeID_L1
    ,COALESCE(inc_per.firstname,per.firstname) as EmployeeFirstName
    ,COALESCE(inc_per.lastname, per.lastname) as EmployeeSurname  
    ,COALESCE(CAST(i2a.PersonLoggedByID as varchar2(100)), 
             log_per.employeeid, 'OTHER_ID') as PersonLoggedByID -- mpatton chloe may want to change default
    ,to_char(inst.email) as TMP_logged_email_address
    ,COALESCE(close_per.internalstaffid, create_per.internalstaffid) as PersonAssignedToID 
    ,DBMS_LOB.SUBSTR(UPPER(substr(risk_ent.entitycollectionname, instr(risk_ent.entitycollectionname,'-',-1)+2 )),100,1) as PersonTypeCode
    ,COALESCE( i2a.WorkActivityCategory,
               i2d.workactivitycategory,
     CAST(
     CASE 
       WHEN REGEXP_LIKE(UPPER(i.activityattime), 
                        '(CADAVER|CLINICAL|HOSPITAL|PATIENT|SURGEON)' )  THEN '001' --'Clinical Work'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(CARRY|LIFT|LOAD|MANUAL HANDL|MOV|PULL|PUSH|RELOCAT|SHIFT|TRANSPORT)(ING)' )  THEN '009' --'Moving Equipment/Manual Handling'
       WHEN REGEXP_LIKE(UPPER(i.activityattime), 
                        '(DRILL|INSTALL|MAINTENANCE)' )  THEN '007' --'Maintenance/Repair/Cleaning'
                        
       WHEN REGEXP_LIKE(UPPER(i.activityattime), 
                        '(COMPUTER|DATABASE|DATA ENTRY|WORKSTATION)' )  THEN '004' --'Computer Use'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(FIELD(| )(TRIP|WORK))' )  THEN '005' --'Fieldwork'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(GARDEN|HEDG)(|ING)' )  THEN '006' --'Gardening'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(DISTILL|EXPERIMENT|LABORATORY|PIPETTE)' )  THEN '002' --'Laboratory Work'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(COFFEE|FOOD|KETTLE| TEA )' )  THEN '017' --'Kitchen/Tea Preparation'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(CLEAN|DRILL_INSTALL|REFURB|REMOV(E|)|REPAIR|STRIP(P|)|TRIM(M|)|VACUUM|WELD)(|ING)' )  THEN '007' --'Maintenance/Repair/Cleaning'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(MEETING)' )  THEN '008' --'Meeting/Conference/Event'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(ADMINISTRATION|COPIER|OFFICE (ACTIVITIES|DUTIES|WORK)|PRINTER|TYPING|WORKPLACE (ASSESSMENT|INSPECTION)|WORK DUTIES)' )  THEN '014' --'Office Work'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '((CYCL|EXERCIS(E|)|PLAY|TREADMILL|GAME)(|ING))' )  THEN '015' --'Sport or Leisure Activity'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(LECTURING|TEACH(|ING))' )  THEN '016' --'Teaching/Lecturing'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(DRIVING|PARKING|REVERSING|RIDING|TRAVELLING)' )  THEN '018' --'Travel related to Work/Study'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(ENTERING|PATHWAY|^RUNNING|WALK(ED|ING))' )  THEN '012' --'Walking'
    
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(SITTING|WORKING.*(DESK|OFFICE|RECORDS))' )  THEN '014' --'Office Work'
       WHEN REGEXP_LIKE(UPPER(i.activityattime),
                        '(N/A|UNKNOWN)' )  THEN '010' --'Other'
    --   ELSE 'Other'
    END AS NVARCHAR2(100) 
    ) ) as WorkActivityCategory    
,COALESCE(i2a.WorkActivityPerformed,
          substr(trim(i.activityattime),1,150), 
          DBMS_LOB.SUBSTR(i.whathappenedunexpectantlydescr,150,1)
    ) as WorkActivityPerformed                                         
    ,null as WorkActivitySubCategory   
,COALESCE( i2a.locationcategory, 
           i2d.locationcategory,
           CAST(
             CASE 
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(STAIRS|STAIR( |)(WAY|WELL)|STEP)' ) AND 
                    UPPER(i.incidentlocation) NOT LIKE '%UPSTAIRS%' THEN '00010' --'Stairs'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(FOOT( |)PATH|PATHWAY|PROMENADE|RAMP|WALK( |)WAY)' ) THEN '00008' --'Pathway/Walkway'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(HIGHWAY|HWY|ROUND.?A.?BOUT)' ) THEN '00009' --'Roads'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(CAR( |)PAR(K| )|PARKING AREA)' ) THEN '00002' --'Carpark'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(^CURTIN STADIUM|STADIUM.*INDOOR.*COURT)' ) THEN '00001' --'Building'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(BBQ|COURT(S|YARD)|GARDEN|GROUNDS)' ) THEN '00004' --'Grounds'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation), 
                                '(BOOKSHOP|BLD|BUILDING|CLINIC|FOYER|HOSPITAL|KITCHEN|LABORATORY|LOUNGE|RECEPTION|ROOM|TAVERN)' ) THEN '00001' --'Building'
               WHEN REGEXP_LIKE(SUBSTR(i.incidentlocation,1,3), 
                                '^[0-9]{3}' ) THEN '00001' --'Building'
               WHEN REGEXP_LIKE(SUBSTR(i.incidentlocation,1,4), 
                                '^B[0-9]{3}' ) THEN '00001' --'Building'
                WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(OFFICE)' ) THEN '00007' --'Office'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(CAT( |)SPACE)' ) THEN '00003' --'CAT Spaces'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(LECTURE THEATRE)' ) THEN '00005' --'Lecture Theatre'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(OFF CAMPUS|FARM)' ) THEN '00006' --'Off Campus'
               WHEN REGEXP_LIKE(UPPER(i.incidentlocation),
                                '(ROAD(S| ))' ) THEN '00009' --'Roads'
            --       ELSE 'No Match'
           END AS NVARCHAR2(100) )
) AS LocationCategory
    ,COALESCE(i2a.LocationDetails,
              DBMS_LOB.SUBSTR(i.incidentlocation,150,1) 
              )as LocationDetails --mpatton
    ,categ.incidentcategoryname as MajorEvent  
--    ,case
--      when stat.incidentstatusname like 'Open%' then 'Open'
--      when stat.incidentstatusname like 'Closed%' then 'Closed'
--      else 'Unknown'
--      end   
    ,'Closed' as Status  -- default all to closed
    ,i.incidentid2 as IncidentShortTitle 
    ,COALESCE(i2a.DateTimeOfIncident,
              inst.occurencedate ) as DateTimeOfIncident
    ,inst.createdate as DateTimeReported
    ,COALESCE(i2a.MOICode, 
              rmss_extract.get_rmss_xref('MECHANISM', moi.mechanismcode, moi.mechanismname)) as MOICode          
    ,COALESCE(i2a.AOICode, 
              rmss_extract.get_rmss_xref('AGENCY', agen.agencyofincidentcode, agen.agencyofincidentname)
             )as AOICode            
--, natr.natureofincidentcode||'*'||natr.natureofincidentname AS OLD_NATURE_VALUES --mpatton   
    ,COALESCE(i2a.NOICode, 
              rmss_extract.get_rmss_xref('NATURE', natr.natureofincidentcode, natr.natureofincidentname), 
              CAST(
              CASE 
                WHEN REGEXP_LIKE(i.whathappenedunexpectantlydescr, '(faint|feint)', 'i') THEN '98'
                WHEN REGEXP_LIKE(i.whathappenedunexpectantlydescr, 'dislocate', 'i') THEN '3'
                WHEN REGEXP_LIKE(i.whathappenedunexpectantlydescr, 'needle', 'i') THEN '8' 
                WHEN REGEXP_LIKE(i.whathappenedunexpectantlydescr, 'psych', 'i') THEN '38'
                WHEN REGEXP_LIKE(i.whathappenedunexpectantlydescr, 'sprain', 'i') THEN '4'
              END AS NVARCHAR2(100) ),
              CASE -- these incident categories will not have an injury, so default to other
                WHEN categ.incidentcategoryname IN ('Near Hit/Miss','Property/Equipment Damage','Spills','Vehicle Event'
                ) THEN n'98' --'Other
              END
              ) as NOICode
    ,COALESCE(i2a.BodilyCode,
              bparts.bodyparts ) as BodilyCode 
    ,COALESCE(i2a.IncidentLocationCode,
              null ) as IncidentLocationCode
    -- when we cannot determine the logged by personID add the logged by email for reference purposes
    ,COALESCE(i2a.riskdescription, 
             DBMS_LOB.SUBSTR(i.whathappenedunexpectantlydescr,1900,1) 
            ||CASE 
               WHEN log_per.employeeid IS NULL THEN '(Logged by email: '||to_char(inst.email)||')' 
               ELSE NULL 
            END )
     as RiskDescription
    ,CASE shift.shiftpercentworkid 
       when n'1' then '25'
       when n'2' then '50'
       when n'3' then '75'
       when n'4' then '100' --'
       --when null then 'NA'
       else 'NA'
      END as ProportionOfWork 
    ,case inv_per.trainingprovidedid  when null then 'FALSE'   else 'TRUE'  end as TrainingRequired
    ,DBMS_LOB.SUBSTR(i.witness,2000,1) as Witness
    ,null as ManagerNotified
    ,null as ManagerNotifiedDateTime
    ,DBMS_LOB.SUBSTR(inc_ans.detail,80,1) as InternalPersonNotified 
    ,null as InternalPersonNotifiedDateTime
    ,null as ExternalPersonNotified
    ,null as ExternalPersonNotifiedDateTime
    ,case inv_per.isinjury when 1 then 'TRUE'   else 'FALSE'  end as Injury
    ,case inv_per.ismedicalrequired   when 1 then 'TRUE'   else 'FALSE'  end as MedicalTreatment
    ,case inv_per.isfirstaidgiven  when 1 then 'TRUE'   else 'FALSE'  end as FirstAid
    ,i2a.gp as GP
    ,i2a.hospital as Hospital
    ,inv_per.firstaidgivenby as MedicalDetails
--, inv_per.islti as orig_islti -- mpatton 
    ,case inv_per.islti when 1 then 'FALSE' else 'TRUE' end as NoTimeLost
    ,case inv_per.islti when 1 then 'TRUE' else 'FALSE' end as LostTimeLost 
    ,'FALSE' as RestrictedWorkDuties
    ,'FALSE' as Fatality
    ,'NA' as SideOfInjury                                                            
    ,case when upper(categ.incidentcategoryname) like '%PROPERTY%DAMAGE%' then 'TRUE' else 'FALSE' end as PropertyDamaged
    ,'FALSE' as OwnAssetProperty
    ,'FALSE' as ThirdPartyAssetProperty
    ,DBMS_LOB.SUBSTR(replace(i.immediateactions,chr(10)),2000,1) as ActionsTaken
    ,null as FurtherActions                                                        
    ,LISTAGG(DBMS_LOB.SUBSTR(prop_dam.description,2000,1), '; ') WITHIN group (order by DBMS_LOB.SUBSTR(prop_dam.description,2000,1) ) over (PARTITION BY prop_dam.incidentid)  as PropertyDamageDetail 
    ,cost_dam.costofdamagename as PropertyDamageCost
    ,null as EmergencyResponse
    ,CASE i.isreportable when 1 then 'TRUE' else 'FALSE' end as WorkSafeNotifiable
    ,ins_sev.text as EarlyNotification
    FROM incident i 
         INNER JOIN incidententitycollection risk_ent ON (risk_ent.incidententitycollectionid = i.riskentitycollectionid)
         INNER JOIN incidentcategory categ ON (categ.incidentcategoryid = i.incidentcategoryid)
         INNER JOIN incidentstatus stat ON (stat.incidentstatusid = i.incidentstatusid)
         INNER JOIN incidentinstance inst ON (inst.incidentid = i.incidentid)
         INNER JOIN incidentseverity ins_sev ON (ins_sev.incidentseverityid = i.incidentseverityid)   
         INNER JOIN person create_per ON (create_per.personid = i.incidentcreatorpersonid)
         LEFT OUTER JOIN involvedperson inv_per ON (i.incidentid = inv_per.incidentid  )
         LEFT OUTER JOIN incidentperson inc_per ON (inc_per.incidentpersonid = inv_per.incidentpersonid
                                                    OR (inc_per.incidentpersonid = i.involvedincidentpersonid AND inv_per.incidentpersonid IS NULL ) -- only use this if inv_per not available)
                                                    )
         LEFT OUTER JOIN person per ON (per.personid = inc_per.personid )         
         LEFT OUTER JOIN person close_per ON (close_per.personid = i.closurepersonid)
         LEFT OUTER JOIN propertydamage prop_dam ON (prop_dam.incidentid = i.incidentid)
         LEFT OUTER JOIN costofdamage cost_dam ON (cost_dam.costofdamageid = i.costofdamageid)
         LEFT OUTER JOIN mechanism moi ON (moi.mechanismid = inv_per.mechanismid)
         LEFT OUTER JOIN agencyofincident agen ON (agen.agencyofincidentid = inv_per.agencyofincidentid)
         LEFT OUTER JOIN natureofincident natr ON (natr.natureofincidentid = inv_per.natureofincidentid)
         LEFT OUTER JOIN (SELECT ibp.involvedpersonid, listagg(substr(bp.bodypartname,1,2), ';') WITHIN group (order by bodypartname)  as bodyparts
                          FROM incidentbodypart ibp 
                          INNER JOIN bodypart bp ON (bp.bodypartid = ibp.bodypartid)
                          GROUP BY ibp.involvedpersonid
                         ) bparts ON (bparts.involvedpersonid = inv_per.involvedpersonid)
         LEFT OUTER JOIN shiftpercentworked shift ON (shift.shiftpercentworkid = inv_per.shiftpercentworkid)
         LEFT OUTER JOIN (incidentanswer inc_ans 
                          INNER JOIN incidentoption inc_opt ON (inc_opt.INCIDENTOPTIONID = inc_ans.INCIDENTOPTIONID AND 
                                                                inc_opt.incidentquestionid = '5974f50a-493b-4797-937c-bc3e2d6d84b0' )-- Q:Has this been reported to your Curtin Contact
                         ) ON (inc_ans.incidentinstanceid = inst.incidentinstanceid )
         LEFT OUTER JOIN RMSS_person_email log_per ON (REPLACE(TRIM(lower(inst.email)),'exchange.','') = TRIM(lower(log_per.email_address))  )  -- this is what slows down the entire query
         LEFT OUTER JOIN RMSS_incident2details i2d ON (i2d.incidentid2 = i.incidentid2 )
         LEFT OUTER JOIN RMSS_incident2all i2a ON (i2a.incidentid2 = i.incidentid2 )
    WHERE 1=1 
    --
--    AND i.INCIDENTID2 IN ( '365')
--    AND i.INCIDENTID2 in ( '709','1707','2145')  
AND i.INCIDENTID2 between  '001' and '2400' --in ( '469')
--and  to_char(inst.email) = 'alexgdavis@bigpond.com'
    --and eoh.ORGLEVEL1CODE is null
    --order by to_number(i.incidentid2)
) level_1          
) level_2
--
LEFT OUTER JOIN RMSS_employee_org_history eoh ON (eoh.rowid = (select rowid 
                                                               from (select rowid, 
                                                                            row_number() over (order by eoh2.start_date desc, eoh2.rowid desc) as rnum
                                                                       from RMSS_employee_org_history eoh2     
                                                                      where eoh2.employeeid = level_2.EmployeeID
                                                                        and level_2.DateTimeOfIncident between eoh2.start_date and eoh2.end_date)
                                                              where rnum = 1 )  )
WHERE 1=1 
--
--AND level_2.LocationCategory = 'Carpark'
--and UPPER(WorkActivityPerformed) like '%RUNNING%'
--AND WorkActivityPerformed is not null
--AND LENGTH(PersonLoggedByID) > 8
--AND PersonLoggedByID != 'ALIAS_HOLD'
--and level_2.employeeid_orig is null and (level_2.EmployeeID_new is not null and level_2.EmployeeID_new != 'MULT')
--and level_2.employeeid_orig is null and level_2.employeefirstname is null and level_2.employeesurname is  null
--and level_2.EmployeeID is not null
--AND LENGTH(level_2.EmployeeID) = 7
--and eoh.location is null
--and (level_2.EmployeeID is null or level_2.EmployeeID like 'MULT%')
--AND LOWER(PersonLoggedByID)  LIKE 'leorocha23@gmail.com'
--and (persontypecode = 'STUDENT' and 
--and persontypecode = 'STAFF'
--and personloggedbyid like '%@%'
--and persontypecode in ('STAFF', 'STUDENT')
--and (Level_2.BodilyCode like '%;%')
--and Level_2.status != 'Closed'
--AND Level_2.orig_islti = '1'
--and Level_2.NoTimeLost = Level_2.LostTimeLost
and Level_2.MajorEvent not in ('Hazard')
order by INCIDENTID2 
--)
--where noicode is null
--
--group by INCIDENTID2 having count(9) > 1
--and LENGTH(EmployeeID) >7

