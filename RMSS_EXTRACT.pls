create or replace PACKAGE RMSS_EXTRACT AS 
  
  function get_rmss_xref(p_kind     in rmss_riskware_xref.kind%type,
                         p_old_code in rmss_riskware_xref.old_code%type,
                         p_old_desc in rmss_riskware_xref.old_description%type) return rmss_riskware_xref.new_code%type;

  function get_rmss_xref_desc(p_kind     in rmss_riskware_xref.kind%type,
                              p_old_desc in rmss_riskware_xref.OLD_DESCRIPTION%type) return rmss_riskware_xref.new_code%type;

  FUNCTION get_location_code(p_parent_loc  IN VARCHAR2,
                             p_loc_details IN VARCHAR2) RETURN rmss_rw_locations.locationcode%TYPE;
                             
  function get_ID_from_name(p_per_type    in varchar2,
                            p_firstname   in person.firstname%type,
                            p_lastname    in person.lastname%type) return person.internalstaffid%type;
                            
  function get_ID_from_name2(p_per_type    in varchar2,
                            p_firstname   in person.firstname%type,
                            p_lastname    in person.lastname%type) return person.internalstaffid%type;
                            
END RMSS_EXTRACT;